﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Cube : MonoBehaviour {

    public Text text;

    public List<int> numbers = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    public int final_number;
    public bool isFinal = false;

    public void OutPut()
    {
        text.text = final_number.ToString();
    }


    public void Input(string newText)
    {
        final_number = int.Parse(newText);
        isFinal = true;
    }

}
