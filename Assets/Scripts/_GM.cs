﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _GM : MonoBehaviour {


    public List<Block> blocks = new List<Block>();

    int[][] posibleCheckNumber = new int[9][];

    int counter = 0;


	void Start () {
		posibleCheckNumber[0] = new int[4] { 1, 2, 3, 6 };
        posibleCheckNumber[1] = new int[4] { 0, 2, 4, 7 };
        posibleCheckNumber[2] = new int[4] { 0, 1, 5, 8 };
        posibleCheckNumber[3] = new int[4] { 0, 4, 5, 6 };
        posibleCheckNumber[4] = new int[4] { 1, 3, 5, 7 };
        posibleCheckNumber[5] = new int[4] { 2, 3, 4, 8 };
        posibleCheckNumber[6] = new int[4] { 0, 3, 7, 8 };
        posibleCheckNumber[7] = new int[4] { 1, 4, 6, 8 };
        posibleCheckNumber[8] = new int[4] { 2, 5, 6, 7 };
    }
	
	
	void Update () {
		
	}


    public void DeleteOutBlock()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            for (int j = 0; j < blocks[i].cubes.Count; j++)
            {
                if(blocks[i].cubes[j].isFinal == true)
                {
                    blocks[i].numbers.Remove(blocks[i].cubes[j].final_number);
                    //Debug.Log("Remove from block " + i + " cubes " + j + " number " + blocks[i].cubes[j].final_number);
                    /*
                    for (int n = 0; n < blocks[i].cubes[j].numbers.Count; n++)
                    {
                        if(blocks[i].cubes[j].numbers[n] == blocks[i].cubes[j].final_number - 1)
                        {
                            blocks[i].numbers.Remove(blocks[i].cubes[j].numbers[n]);
                        }
                    }
                    */
                    for (int n = 0; n < blocks[i].cubes.Count; n++)
                    {
                        if(!blocks[i].cubes[n].isFinal)
                        {
                            blocks[i].cubes[n].numbers.Remove(blocks[i].cubes[j].final_number);
                        }
                        
                    }
                }
            }
        }
        Debug.Log("Delete out Block");
        /*
        for (int i = 0; i < blocks[1].cubes[2].numbers.Count; i++)
        {
            Debug.Log("numbers on cube: " + blocks[1].cubes[2].numbers[i]);
        }
        */
    }

   void CheckNumbers()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            Debug.Log("for block");
            for (int j = 0; j < blocks[i].cubes.Count; j++)
            {
                Debug.Log("for cube j = " + j);
                if (!blocks[i].cubes[j].isFinal)  
                 {
                    for (int o = 0; o < blocks[i].cubes[j].numbers.Count; o++)
                    {
                        Debug.Log("for number o = " + o);
                        if (blocks[i].cubes[j].numbers.Count == 1)
                        {
                            Debug.Log("if count 1");
                            blocks[i].cubes[j].final_number = blocks[i].cubes[j].numbers[0];
                            blocks[i].cubes[j].isFinal = true;
                            break;
                        }
                        else if (blocks[i].cubes[j].numbers.Count > 1)
                        {
                            Debug.Log("else count more than 1");
                            for (int a = 0; a < posibleCheckNumber[i].Length; a++)
                            {
                                //if (blocks[posibleCheckNumber[i][a]].cubes[posibleCheckNumber[j][a]].isFinal)
                                //{
                                Debug.Log("for cubesPosibles");
                                if (a < 2)
                                {
                                    if (blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[posibleCheckNumber[j][0]].final_number ||
                                        blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[posibleCheckNumber[j][1]].final_number ||
                                        blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[j].final_number)
                                    {
                                        Debug.Log("if posible check");
                                        blocks[i].cubes[j].numbers.Remove(o);
                                    }
                                }
                                else if (a >= 2)
                                {
                                    if (blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[posibleCheckNumber[j][2]].final_number ||
                                        blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[posibleCheckNumber[j][3]].final_number ||
                                        blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[j].final_number)
                                    {
                                        Debug.Log("if posible check");
                                        blocks[i].cubes[j].numbers.Remove(o);
                                        
                                    }
                                }


                                /*
                                Debug.Log("for posiblecheck");
                                if (blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[posibleCheckNumber[j][a]].final_number ||
                                    blocks[i].cubes[j].numbers[o] == blocks[posibleCheckNumber[i][a]].cubes[j].final_number)
                                {
                                    Debug.Log("if posible check");
                                    blocks[i].cubes[j].numbers.Remove(o);
                                }

                                else
                                {
                                    Debug.Log("Not eguals ");
                                }
                                */
                                //}
                            }
                        }
                     }
                }
                else
                {
                    Debug.Log("not 1");
                }
            }

        }
        IsBlockFull();
        counter++;
        Debug.Log("CheckNumber");
    }

    private void IsBlockFull()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            for (int j = 0; j < blocks[i].cubes.Count; j++)
            {
                if (blocks[i].cubes[j].isFinal)
                {
                    Debug.Log("Blocks full");
                    Output();
                }

                else
                {
                    if (counter <= 20)
                    {
                        CheckNumbers();
                    }
                    else
                    {
                        Debug.Log("Check is more than 20");
                    }
                }
            }
        }
    }

    private void  Output()
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            for (int j = 0; j < blocks[i].cubes.Count; j++)
            {
                blocks[i].cubes[j].OutPut();
            }
        }
        Debug.Log("Output");
    }


   public void GO()
    {
        DeleteOutBlock();
        CheckNumbers();
        Debug.Log("Go");
    }


}
